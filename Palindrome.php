<?php
class Palindrome
{
    public static function isPalindrome($word)
    {
        $tmp = strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $word));
        if(strrev($tmp)==$tmp)
            return TRUE;
        else
            return FALSE;
    }
}

echo Palindrome::isPalindrome('Deleveled');